#---- Generator function ----
sample_ <- function(type = c("srs", "_1")) {
  # match sampling function
  sample_fun <- switch(
    match.arg(type), 
    "srs" = sample,
    "_1" = function(x, t, replace) 1
    )
  function(pop_size, sample_size, replacement = FALSE) {
    # check input
    stopifnot(
      "'pop' must be an atomic vector" = 
        is.vector(pop_size, "numeric"),
      "'size' must be a numeric vector" = 
        is.vector(sample_size, "numeric"),
      "'pop_size' and 'sample_size' must be the same length" = 
        length(pop_size) == length(sample_size),
      "'replacement' must be TRUE or FALSE" = 
        length(replacement) == 1L && is.logical(replacement) && !is.na(replacement)
    )
    # draw sample for each element pop/sample pair
    Map(sample_fun, pop_size, sample_size, replacement)
  }
}

#---- SRS sample ----
sample_srs <- sample_("srs")

#---- Trivial sample ----
sample_1 <- sample_("_1")
